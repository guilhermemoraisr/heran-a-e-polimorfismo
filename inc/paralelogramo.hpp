#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica { 
    private:
        float base2; // o paralelogramo possui duas bases diferentes.
    public:
        Paralelogramo();
        Paralelogramo(string tipo, float base, float altura, float base2);
        ~Paralelogramo();
        float calcula_area();
        float calcula_perimetro();
        void set_base2(float base2);
        float get_base2();
};

#endif
