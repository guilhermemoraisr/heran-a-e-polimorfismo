#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica {
    public:
        Hexagono(); // construtor
        Hexagono(string tipo, float base);
        ~Hexagono(); // destrutor
        float calcula_area();
        float calcula_perimetro();
};

#endif
