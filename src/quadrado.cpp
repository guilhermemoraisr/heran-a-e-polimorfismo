#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(){
    set_tipo("quadrado");
    set_base(5.0);
    set_altura(get_base());
}
Quadrado::Quadrado(string tipo, float base, float altura){
    if(base != altura) // A fórmula é a mesma da FormaGeometrica.
        throw(1);
    set_tipo(tipo);
    set_base(base);
    set_altura(base);
}
Quadrado::~Quadrado(){
}
