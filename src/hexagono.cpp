#include "hexagono.hpp"
#include <iostream>
#include <math.h> //adicionada por conta do expoente, ou usar a biblioteca bits.

Hexagono::Hexagono(){
    set_tipo("hexágono");
    set_base(5.0);
}
Hexagono::~Hexagono(){    
}
Hexagono::Hexagono(string tipo, float base){
    set_tipo(tipo);
    set_base(base);
}
float Hexagono::calcula_area(){
    return (3*sqrt(3)*pow(get_base(),2))/2;
}
float Hexagono::calcula_perimetro(){
    return 6*get_base();
}
