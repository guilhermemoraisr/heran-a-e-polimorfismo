#include <iostream>
#include "circulo.hpp"
#include "formageometrica.hpp"
#include "hexagono.hpp"
#include "paralelogramo.hpp"  // 6 classes solicitadas + formageometrica genérica.
#include "pentagono.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"

#include <vector>

using namespace std;

int main(int argc, char const *argv[])
{
    Circulo circulo;
    FormaGeometrica formageometrica;
    Hexagono hexagono;
    Paralelogramo paralelogramo;
    Pentagono pentagono;
    Quadrado quadrado;        
    Triangulo triangulo;

    vector <FormaGeometrica *> formas; // lista de objetos

    formas.push_back(new FormaGeometrica(2.0, 3.0));
    formas.push_back(new Triangulo("triângulo", 12.0, 8.0));
    formas.push_back(new Quadrado("quadrado", 5.0, 5.0));
    formas.push_back(new Circulo("círculo", 5.0));
    formas.push_back(new Paralelogramo("paralelogramo", 7.0, 5.0, 3.0));
    formas.push_back(new Pentagono("pentágono", 7.0, 8.0));
    formas.push_back(new Hexagono("hexágono", 6.0));

    for(FormaGeometrica * saida: formas){  // laço que imprime todos os tipos área e perí­metro dos objetos.
        cout << "tipo = " << saida->get_tipo() << endl;
        cout << "Área = " << saida->calcula_area() << endl;
        cout << "perí­metro = " << saida->calcula_perimetro() << endl << endl;
    }

    return 0;
}
