#include "circulo.hpp"
#include <iostream>

float pi = 3.14;

Circulo::Circulo(){
    set_tipo("círculo");
    raio = 5.0;
}
Circulo::~Circulo(){
   
}
Circulo::Circulo(string tipo, float raio){
    this->raio = raio;
    set_tipo(tipo);
}
float Circulo::calcula_area(){
    return raio*raio*pi;
}
float Circulo::calcula_perimetro(){
    return 2*pi*raio;
}
