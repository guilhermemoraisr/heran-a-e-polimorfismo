#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
    set_tipo("paralelogramo");
    set_base(7.0);
    base2 = 5.0;
    set_altura(3.0);
}
Paralelogramo::~Paralelogramo(){
}
Paralelogramo::Paralelogramo(string tipo, float base, float base2, float altura){
    set_tipo(tipo);
    set_base(base);
    this->base2 = base2;
    set_altura(altura);
}
float Paralelogramo::calcula_area(){
    return 2*get_base()+2*base2;
}
float Paralelogramo::calcula_perimetro(){
    return get_base()*get_altura();
}
